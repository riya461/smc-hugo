
REDIRECTS_CONTENT='
/blogs/:slug     https://blog.smc.org.in/:slug    301
'

STATIC_DIR="../static"

echo "$REDIRECTS_CONTENT" > _redirects

mv _redirects "$STATIC_DIR"

echo "Created _redirects file and moved it to $STATIC_DIR"
