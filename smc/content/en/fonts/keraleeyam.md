+++
name = 'Keraleeyam'
description = 'Keraleeyam is a Malayalam typeface designed by Hussain K H.'
variants = ['Display']
fontWeights = [400]
weight = 400
author = 'Hussain K H'
license = '[SIL Open Font License](http://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/keraleeyam'
availableOnGoogleFonts = true
aliases = ['/fonts/keraleeyam']
page = 'big'
downloadlink = '/downloads/fonts/keraleeyam/keraleeyam.zip'
link = '/keraleeyam'
sampletext = 'നീയും ഞാനും മുമ്പുള്ളവര്‍ പറഞ്ഞ, പിന്മുറക്കാര്‍ പറയാന്‍ പോകുന്ന, പ്രേമനൈരാശ്യങ്ങളിലെ നിഴലുകളാവരുതെന്ന് എനിക്കു നിര്‍ബന്ധമുണ്ട്.'
defaultfontsize = 48
defaultlineheight = 1.1
+++

```
This font is no longer maintained by SMC Project.
Here you can find the last released version.
```

## History

Released on Dec 18,2014.
