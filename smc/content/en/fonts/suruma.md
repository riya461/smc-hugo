+++
name = 'Suruma'
description = 'Malayalam font designed by Suresh P'
weight = 0
author = 'Suresh P'
variants = ['Regular']
fontWeights = [400]
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/suruma'
aliases = ['/fonts/suruma']
link = '/suruma'
page = 'normal'
downloadlink = '/downloads/fonts/suruma/suruma.zip'
sampletext = """സുറുമയെഴുതിയ മിഴികളെ
പ്രണയമധുര തേന്‍ തുളുമ്പും
സൂര്യകാന്തി പൂക്കളേ
ജാലകത്തിരശ്ശീല നീക്കി
ജാലമെറിയുവതെന്തിനോ
തേന്‍ പുരട്ടിയ മുള്ളുകള്‍
നീ കരളിലെറിയുവതെന്തിനോ"""
defaultfontsize = 48
defaultlineheight = 1.3
+++

## History

Released on July 23, 2016. Designed by Suresh P.

This font is no longer maintained by SMC Project. Here you can find the last released version.
