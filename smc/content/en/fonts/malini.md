+++
name = 'Malini'
description = 'Malini is a variable font designed by Santhosh Thottingal'
weight = 10
author = 'Santhosh Thottingal'
variableFont = true
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/malini'
page = 'all'

link = '/malini'
downloadlink = '/downloads/fonts/malini/Malini.zip'
aliases = ['/fonts/malini']
images = ['/static/images/specimen/malini-1.svg', '/static/images/specimen/malini-2.svg', '/static/images/specimen/malini-3.svg', '/static/images/specimen/malini-4.svg', '/static/images/specimen/malini-5.svg',  '/static/images/specimen/malini-6.svg', '/static/images/specimen/malini-7.svg']
sampletext="""
പണ്ട്, പറന്നു പറന്ന് ചിറകുകടയുന്ന നാഗത്താന്മാർ പനങ്കുരലിൽ മാണിക്യമിറക്കിവെച്ചു ക്ഷീണം തീർക്കാറുണ്ടായിരുന്നു. നാഗത്താന്മാർക്കായി പനകേറ്റക്കാരൻ കള്ളു നേർന്നു വെച്ചു. പനഞ്ചോട്ടിലാകട്ടെ, അവൻ കുലദൈവങ്ങൾക്കു തെച്ചിപ്പൂ നേർന്നിട്ടു. ദൈവങ്ങളെയും പിതൃക്കളെയും ഷെയ്ഖ് തമ്പുരാനെയും സ്‌മരിച്ചേ പന കയറുകയുള്ളൂ. കാരണം, പിടിനിലയില്ലാത്ത ആകാശത്തിലേക്കാണ് കയറിപ്പോകുന്നത്. പനമ്പട്ടകളിൽ ഇടിമിന്നലും കാറ്റുമുണ്ട്. പനയുടെ കൂർത്ത ചിതമ്പലുകളിലാണെങ്കിൽ തേളുകളുമുണ്ട്. ആ ചിതമ്പലുകളിലുരഞ്ഞു പനകേറ്റക്കാരന്റെ കയ്യും മാറും തഴമ്പു കെട്ടും.
"""
defaultfontsize = 24
defaultlineheight = 1.4
defaultweight = 400

[variables]
  [variables.weight]
    label = 'Weight'
    propName = 'wght'
    min = 100
    max = 900
    step = 1
    default = 400
  [variables.slant]
    label = 'Slant'
    propName = 'slnt'
    min = -12.0
    max = 0
    step = 1
    default = 0.0
  [variables.width]
    label = 'Width'
    propName = 'wdth'
    min = 75.0
    max = 125.0
    step = 1
    default = 100.0

+++
