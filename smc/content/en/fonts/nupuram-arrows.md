+++
name = 'Nupuram-Arrows-Color'
description = 'Educational font to learn writing in Malayalam, to practice in worksheets, first time in Malayalam unicode fonts. You can customize the color using ColrV1 technology.'
weight = 10
title="nupuram"
fontWeights = [400]
variableFont = true
author = 'Santhosh Thottingal'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/nupuram'
availableOnGoogleFonts = true
aliases = ['/fonts/nupuram-arrows']
page = 'big'
downloadlink = '/downloads/fonts/nupuram/Nupuram-Arrows-Color.zip'
link = '/nupuram-arrows'
images = ['/static/images/specimen/nupuram-arrows-1.svg', '/static/images/specimen/nupuram-arrows-2.svg']
sampletext="""നീലവാർമുകിലോരം"""
defaultfontsize = 144
defaultlineheight = 1.2
defaultweight = 400
+++

```
# Educational font.
A font to learn writing in Malayalam, to practice in worksheets, first time in Malayalam unicode fonts.
You can customize the color using ColrV1 technology.
