+++
name = 'Rachana'
description = 'Rachana is a Malayalam typeface designed by Hussain K H'
variants = ['Regular','Bold']
fontWeights = [400, 700]
author = 'Hussain K H'
weight = 0
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/rachana'
aliases = ['/fonts/rachana']
page = 'all'
downloadlink = '/downloads/fonts/rachana/rachana.zip'
link = '/rachana'
images = ['/static/images/specimen/rachana-1.svg', '/static/images/specimen/rachana-2.svg', '/static/images/specimen/rachana-3.svg', '/static/images/specimen/rachana-4.svg', '/static/images/specimen/rachana-5.svg']
sampletext="""
പണ്ട്, പറന്നു പറന്ന് ചിറകുകടയുന്ന നാഗത്താന്മാർ പനങ്കുരലിൽ മാണിക്യമിറക്കിവെച്ചു ക്ഷീണം തീർക്കാറുണ്ടായിരുന്നു. നാഗത്താന്മാർക്കായി പനകേറ്റക്കാരൻ കള്ളു നേർന്നു വെച്ചു. പനഞ്ചോട്ടിലാകട്ടെ, അവൻ കുലദൈവങ്ങൾക്കു തെച്ചിപ്പൂ നേർന്നിട്ടു. ദൈവങ്ങളെയും പിതൃക്കളെയും ഷെയ്ഖ് തമ്പുരാനെയും സ്‌മരിച്ചേ പന കയറുകയുള്ളൂ. കാരണം, പിടിനിലയില്ലാത്ത ആകാശത്തിലേക്കാണ് കയറിപ്പോകുന്നത്. പനമ്പട്ടകളിൽ ഇടിമിന്നലും കാറ്റുമുണ്ട്. പനയുടെ കൂർത്ത ചിതമ്പലുകളിലാണെങ്കിൽ തേളുകളുമുണ്ട്. ആ ചിതമ്പലുകളിലുരഞ്ഞു പനകേറ്റക്കാരന്റെ കയ്യും മാറും തഴമ്പു കെട്ടും.
"""
defaultfontsize = 24
defaultlineheight = 1.4
defaultweight = 400
+++
