+++
name = 'Meera'
description = 'Meera is a Malayalam typeface designed by Hussain K H'
variants = ['Regular']
weight = 0
fontWeights = [ 400]
author = 'Hussain K H'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/meera'
availableOnGoogleFonts = true
aliases = ['/fonts/meera']
page = 'normal'
downloadlink = '/downloads/fonts/meera/meera.zip'
link = '/meera'
sampletext="""
ആരൊരാളെൻ കുതിരയെ കെട്ടുവാൻ
ആരൊരാളതിൻ മാർഗ്ഗം മുടക്കുവാൻ ?

ദിഗ്വിജയത്തിനെൻ സർഗ്ഗശക്തിയാ‌‌-
മിക്കുതിരയെ വിട്ടയയ്ക്കുന്നു ഞാൻ!
വിശ്വസംസ്കാരവേദിയിൽ പുത്തനാ-
മശ്വമേധം നടത്തുകയാണു ഞാൻ!

നിങ്ങൾ കണ്ടോ ശിരസ്സുയർത്തിപ്പായു-
മെൻ കുതിരയെ, ചെമ്പൻ കുതിരയെ?
"""
defaultfontsize = 44
defaultlineheight = 1.4
defaultweight = 400
+++

```
This font is no longer maintained by SMC Project.
Here you can find the last released version.
```

## History

Released on September 21, 2007.
