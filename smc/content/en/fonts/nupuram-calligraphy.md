+++
name = 'Nupuram-Calligraphy'
description = 'Calligraphy style Malayalam font, designed by Santhosh Thottingal'
weight = 10
title="nupuram"
variableFont = true
author = 'Santhosh Thottingal'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/nupuram'
availableOnGoogleFonts = true
aliases = ['/fonts/nupuram-calligraphy']
page = 'big'
downloadlink = '/downloads/fonts/nupuram/Nupuram-Calligraphy.zip'
link = '/nupuram-calligraphy'
images = ['/static/images/specimen/nupuram-calligraphy-1.svg']
sampletext="""നീലവാർമുകിലോരം"""
defaultfontsize = 144
defaultlineheight = 1.2
defaultweight = 400
[variables]
  [variables.weight]
    label = 'Weight'
    propName = 'wght'
    min = 100
    max = 900
    step = 1
    default = 400
+++
