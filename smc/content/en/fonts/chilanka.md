+++
name = 'Chilanka'
description = 'A handwriting style font for Malayalam, designed by Santhosh Thottingal'
variants = ['Regular']
fontWeights = [400]
weight = 0
author = 'Santhosh Thottingal'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/manjari'
availableOnGoogleFonts = true
aliases = ['/fonts/chilanka']
page = 'normal'
downloadlink = '/downloads/fonts/chilanka/chilanka.zip'
link = '/chilanka'
images = ['/static/images/specimen/chilanka-1.svg', '/static/images/specimen/chilanka-2.svg',]
sampletext="""
ആരെക്കുറിച്ചും ഒന്നും പറയാനില്ലാത്തപ്പോൾ കുപ്പുവച്ചൻ മീൻ പിടിയ്ക്കാനിറങ്ങും. ഒറ്റലുംകൊണ്ട് തോട്ടിലും ചേറ്റിലും ഞാറക്കൊറ്റിയെപ്പോലെ കൊക്കിനടക്കുന്ന കുപ്പുവച്ചനെ വെട്ടിച്ചുപോകാൻ പരൽമീനുകൾക്കു പോലും വിഷമമുണ്ടായിരുന്നില്ല. ജീവനും ഓജസ്സുമുള്ളതത്രയും തന്നെ വെട്ടിച്ചു കടന്നുകളയുന്നു. എങ്കിലും മീൻപിടുത്തം ഒരാശ്വാസമാണ്. വെള്ളത്തിലും ചേറ്റിലും നിഴലിച്ചുകണ്ട ആകാശത്തിനു മീതേ സഞ്ചരിക്കുമ്പോൾ, ശക്തവും സമൃദ്ധവുമായൊരു ജീവിതത്തിന്റെ മങ്ങിയ ഓർമ്മകൾ പരൽമീനുകളെപ്പോലെ കുപ്പുവച്ചനെ തൊട്ടും ഉരസിയും കടന്നു പോയി.
"""
defaultfontsize = 24
defaultlineheight = 1.4
defaultweight = 400
+++

## History

Released on October 27, 2014. [Announcement](https://blog.smc.org.in/new-handwriting-style-font-for-malayalam-chilanka)
