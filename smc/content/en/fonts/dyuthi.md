+++
name = 'Dyuthi'
description = 'Dyuthi is a Malayalam typeface designed by Hiran Venugopal.'
variants = ['Regular']
fontWeights = [400]
weight = 0
author = 'Hiran Venugopal'
license = '[SIL Open Font License](http://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/dyuthi'
availableOnGoogleFonts = true
aliases = ['/fonts/dyuthi']
page = 'big'
downloadlink = '/downloads/fonts/dyuthi/dyuthi.zip'
link = '/dyuthi'
sampletext="""
ആരൊരാളെൻ കുതിരയെ കെട്ടുവാൻ
ആരൊരാളതിൻ മാർഗ്ഗം മുടക്കുവാൻ ?

ദിഗ്വിജയത്തിനെൻ സർഗ്ഗശക്തിയാ‌‌-
മിക്കുതിരയെ വിട്ടയയ്ക്കുന്നു ഞാൻ!
വിശ്വസംസ്കാരവേദിയിൽ പുത്തനാ-
മശ്വമേധം നടത്തുകയാണു ഞാൻ!

നിങ്ങൾ കണ്ടോ ശിരസ്സുയർത്തിപ്പായു-
മെൻ കുതിരയെ, ചെമ്പൻ കുതിരയെ?
"""
defaultfontsize = 44
defaultlineheight = 1.4
defaultweight = 400
+++
