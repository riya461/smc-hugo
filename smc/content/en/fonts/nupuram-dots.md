+++
name = 'Nupuram-Dots'
description = 'Educational font to learn writing in Malayalam, to practice in worksheets, first time in Malayalam unicode fonts.'
weight = 10
title="nupuram"
fontWeights = [400]
variableFont = true
author = 'Santhosh Thottingal'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/nupuram'
availableOnGoogleFonts = true
aliases = ['/fonts/nupuram-dots']
page = 'big'
downloadlink = '/downloads/fonts/nupuram/Nupuram-Dots.zip'
link = '/nupuram-dots'
images = ['/static/images/specimen/nupuram-dots-1.svg', '/static/images/specimen/nupuram-dots-2.svg']
sampletext="""നീലവാർമുകിലോരം"""
defaultfontsize = 144
defaultlineheight = 1.2
defaultweight = 400
+++
