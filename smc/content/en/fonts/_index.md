+++
aliases = ['/fonts']
name = 'Fonts'
description = 'Malayalam fonts maintained by SMC'
+++

##  Other Malayalam fonts

Other than the fonts listed above, which are maintained by SMC, there are a few more free licensed fonts designed and released by other projects and people.

* [Noto Sans Malayalam by Google](https://www.google.com/get/noto/)
* [Baloo Chettan 2 by Ek Type, commissioned by Google fonts.](https://fonts.google.com/specimen/Baloo+Chettan+2?subset=malayalam)
* [Anek Malayalam by Ek Type](https://github.com/EkType/Anek)
* [Sundar, TNJoy, Ezhuthu, Panmana, Karuna etc by Rachana Institute of Typography](http://rachana.org.in/)
