+++
name = 'Gayathri'
description = 'Gayathri - Malayalam typeface designed by Binoy Dominic.'
variants = ['Regular']
weight = 10
fontWeights = [100, 400, 700]
author = 'Binoy Dominic'
license = '[SIL Open Font License](http://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/gayathri'
availableOnGoogleFonts = true
aliases = ['/fonts/gayathri']
page = 'all'
downloadlink = '/downloads/fonts/gayathri/gayathri.zip'
link = '/gayathri'
sampletext="""
ആരൊരാളെൻ കുതിരയെ കെട്ടുവാൻ
ആരൊരാളതിൻ മാർഗ്ഗം മുടക്കുവാൻ ?

ദിഗ്വിജയത്തിനെൻ സർഗ്ഗശക്തിയാ‌‌-
മിക്കുതിരയെ വിട്ടയയ്ക്കുന്നു ഞാൻ!
വിശ്വസംസ്കാരവേദിയിൽ പുത്തനാ-
മശ്വമേധം നടത്തുകയാണു ഞാൻ!

നിങ്ങൾ കണ്ടോ ശിരസ്സുയർത്തിപ്പായു-
മെൻ കുതിരയെ, ചെമ്പൻ കുതിരയെ?
"""
defaultfontsize = 36
defaultlineheight = 1.4
defaultweight = 400
+++



## History
Released on Feb 21,2019.
