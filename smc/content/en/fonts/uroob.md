+++
name = 'Uroob'
description = 'Uroob is a Malayalam typeface designed by Hussain K H'
weight = 10
author = 'Hussain K H'
fontWeights = [400]
variants = ['Regular']
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/uroob'
aliases = ['/fonts/uroob']
link = '/uroob'
page = 'big'
downloadlink = '/downloads/fonts/uroob/uroob.zip'
sampletext = 'വീണക്കമ്പിതകർന്നാലെന്തേ വിരലിൻ തുമ്പുമുറിഞ്ഞാലെന്തേ ഗാനമേ നിൻ മധുവർഷത്താൽ ഞാനലിഞ്ഞു പോയീ'
defaultfontsize = 60
defaultlineheight = 1
+++
## History
Released on July 23, 2016