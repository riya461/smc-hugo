+++
name = 'RaghuMalayalamSans'
variants = ['Regular']
fontWeights = [400]
author = 'Hussain K H, Prof. R. K. Joshi (TypeFont Design Director, Visiting Design Specialist at C-DAC Mumbai) in association with Mr. Rajith Kumar K. M. (TypeFont Designer), assisted by Mr. Nirmal Biswas, Ms. Jui Mhatre and Ms. Supriya Kharkar at C-DAC Mumbai (formerly NCST).'
weight = 0
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/raghu'
aliases = ['/fonts/raghu', '/fonts/raghumalayalamsans']
link = '/raghu'
page = 'normal'
downloadlink = '/downloads/fonts/raghumalayalamsans/raghumalayalamsans.zip'
sampletext="""
ഇത്രനാള്‍ നാമിണങ്ങി പരസ്പര,-
മത്രമാത്രം പ്രപഞ്ചം മധുരിതം
അത്രമാത്രമേ നമ്മുടെ ജീവനു-
മര്‍ത്ഥമുള്ളെന്‍ പ്രിയങ്കരതാരമേ!

കെ. അയ്യപ്പപ്പണിക്കര്‍
"""
defaultfontsize = 44
defaultlineheight = 1.4
defaultweight = 400
+++

## Supported Languages

- Latin
- Malayalam
