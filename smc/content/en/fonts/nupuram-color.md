+++
name = 'Nupuram-Color'
description = 'Nupuram Color font designed by Santhosh Thottingal.'
title="nupuram"
weight = 10
variableFont = true
author = 'Santhosh Thottingal'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/nupuram'
availableOnGoogleFonts = true
aliases = ['/fonts/nupuram-color']
page = 'big'
downloadlink = '/downloads/fonts/nupuram/Nupuram-Color.zip'
link = '/nupuram-color'
images = ['/static/images/specimen/nupuram-color-1.svg', '/static/images/specimen/nupuram-color-2.svg']
sampletext="""നീലവാർമുകിലോരം"""
defaultfontsize = 144
defaultlineheight = 1.2
defaultweight = 400
[variables]
  [variables.weight]
    label = 'Weight'
    propName = 'wght'
    min = 100
    max = 900
    step = 1
    default = 400
+++
