+++
name = 'Nupuram'
description = 'Nupuram is a variable font with four axes: weight, width, softness and slant. It is designed by Santhosh Thottingal.'
weight = 10
author = 'Santhosh Thottingal'
fontWeights = [100, 400, 700]
variableFont = true
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/nupuram'
downloadlink = '/downloads/fonts/nupuram/Nupuram.zip'
aliases = ['/fonts/nupuram']
page = 'all'
link = '/nupuram'
images = ['/static/images/specimen/nupuram-1.svg', '/static/images/specimen/nupuram-2.svg', '/static/images/specimen/nupuram-3.svg', '/static/images/specimen/nupuram-4.svg']
sampletext="""
പണ്ട്, പറന്നു പറന്ന് ചിറകുകടയുന്ന നാഗത്താന്മാർ പനങ്കുരലിൽ മാണിക്യമിറക്കിവെച്ചു ക്ഷീണം തീർക്കാറുണ്ടായിരുന്നു. നാഗത്താന്മാർക്കായി പനകേറ്റക്കാരൻ കള്ളു നേർന്നു വെച്ചു. പനഞ്ചോട്ടിലാകട്ടെ, അവൻ കുലദൈവങ്ങൾക്കു തെച്ചിപ്പൂ നേർന്നിട്ടു. ദൈവങ്ങളെയും പിതൃക്കളെയും ഷെയ്ഖ് തമ്പുരാനെയും സ്‌മരിച്ചേ പന കയറുകയുള്ളൂ. കാരണം, പിടിനിലയില്ലാത്ത ആകാശത്തിലേക്കാണ് കയറിപ്പോകുന്നത്. പനമ്പട്ടകളിൽ ഇടിമിന്നലും കാറ്റുമുണ്ട്. പനയുടെ കൂർത്ത ചിതമ്പലുകളിലാണെങ്കിൽ തേളുകളുമുണ്ട്. ആ ചിതമ്പലുകളിലുരഞ്ഞു പനകേറ്റക്കാരന്റെ കയ്യും മാറും തഴമ്പു കെട്ടും."""
defaultfontsize = 24
defaultlineheight = 1.7
defaultweight = 350
[variables]
  [variables.slant]
    label = "Slant"
    propName = "slnt"
    min = -15.0
    max = 0
    step = 0.1
    default = 0.0
  [variables.width]
    label = "Width"
    propName = "wdth"
    min = 75.0
    max = 125.0
    step = 0.1
    default = 100.0
  [variables.softness]
    label = "Soft"
    propName = "soft"
    min = 0.0
    max = 100.0
    step = 0.1
    default = 50.0


+++

## Find the right width, weight, & slant that fits you.

Variable fonts give you granular control of fonts are displayed by packaging a set of variable axes into one single font file.
Nupuram come with four variable axes each: weight (thin to black), width (condensed to expanded), soft(sharp to supersoft) and slant (regular to italics). By combining these axes freely, you have access to thousands of different looks, and endless of possibilities.

### Fine-typographic control
Adjust font parameters in small increments for ultimate control without compromise.

### Small font file sizes
Reduce webfont file sizes and increase the performance of your website by using a single variable webfont throughout.

### Text animations
Stylistic variation of Nupuram are continuous, and can be animated for text transitions and special effects.

### Optimal legibility
Nupuram contain size-specific variations which make the text superbly legible, optimised for any size.

### Responsive type
Variable fonts assist in tuning type according to different formats, resolutions and display sizes.