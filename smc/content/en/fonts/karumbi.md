+++
name = 'Karumbi'
description = 'Karumbi is a Malayalam typeface designed by Kevin Siji. It is a handwriting style font'
variants = ['Regular']
fontWeights = [400]
weight = 0
author = 'Kevin Siji'
license = '[SIL Open Font License](http://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/karumbi'
availableOnGoogleFonts = false
aliases = ['/fonts/karumbi']
page = 'normal'
downloadlink = '/downloads/fonts/karumbi/karumbi.zip'
link = '/karumbi'
sampletext = """ഓമലാളെ കണ്ടൂ ഞാൻ പൂങ്കിനാവിൽ
താരകങ്ങൾ പുഞ്ചിരിച്ച നീലരാവിൽ.
നാലുനിലപന്തലിട്ടു വാനിലമ്പിളി
നാഗസ്വരമേളമിട്ടു പാതിരാക്കിളി.
ഏകയായി രാഗലോലയായി
എന്റെ മുന്നിൽ വന്നവൾ കുണുങ്ങിനിന്നു"""
defaultfontsize = 48
defaultlineheight = 1.1
+++
