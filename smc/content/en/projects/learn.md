+++
title = 'Learn to write Malayalam'
draft = false
val = 'Fonts & Script'
img = 'none'
details = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
use = 'https://learn.smc.org.in/'
source = 'https://gitlab.com/smc/mlmash'
+++
A simple application to observe and learn how to write Malayalam letters. 