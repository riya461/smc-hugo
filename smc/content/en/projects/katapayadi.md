+++
title = 'Katapayadi'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/katapayadi'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'

summary="Katapayadi writing method"
+++

Katapayadi sankhya is a simplification of Aryabhata's Sanskrit numerals, due probably to Haridatta from Kerala. In Malayalam it is also known as 'Paralperu'. For eg: ചണ്ഡാംശുചന്ദ്രാധമകുംഭിപാല represents 31415926536 which is π*1000000000000000. More examples in Malayalam are given in this page LibIndic's katapayadi module may be used to find out katapayadi number of a given string, as well as to get Swarasthanas of a Melakartha number. 