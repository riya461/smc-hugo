+++
title = 'chardetails'
draft = false
val = 'lib'
img = 'none'
source = 'https://github.com/libindic/chardetails'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'
+++
LibIndic's chardetails module may be used to get the details of a given unicode character. 