+++
title = 'Shingling'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/shingling'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'

summary ="Indic W-shingling Library"
+++


A w-shingling is a set of unique "shingles"—contiguous subsequences of tokens in a document—that can be used to gauge the similarity of two documents. The w denotes the number of tokens in each shingle in the set. This library supports English, Hindi, Malayalam, Kannada and Bengali. 