+++
title = 'Manglish'
draft = false
val = 'Mobile'
img = '/images/amma.svg'
source = 'none'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='https://play.google.com/store/apps/details?id=subins2000.manglish'
fdroid='https://fdroid.org/packages/subins2000.manglish/'
howto='none'
download='none'
doc='none'
details = 'https://subinsb.com/manglish/'

+++
A simple app to convert Malayalam text to Manglish. For people who understands Malayalam, but can't read the script. 