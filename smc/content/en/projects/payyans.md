+++
title = 'Payyans'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/payyans'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'

summary="ASCII - Unicode converter "
+++

LibIndic's Payyans module may be used to convert texts encoded in ASCII format to Unicode and vice-versa. More fonts can be added by placing their maps easily. 