+++
title = 'ucasort'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/ucasort'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'

summary="Module to sort words basded on linguistics"
+++

Unicode Collation Algorithm(UCA) based sorting for all languages defined in Unicode. The collation weights used in this application is a modified version of Default Unicode Collation Element Table (DUCET). It use the default weights defined by Unicode. Malayalam and Tamil sorting is compatible with GNU C library collation definition. 