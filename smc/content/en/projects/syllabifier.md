+++
title = 'Syllabifier'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/syllabalizer'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'
+++
LibIndic's syllabifier module may be used to split words into their constituent syllables. It currently works for Malayalam, Kannada, Bengali, Tamil, Hindi and English. 