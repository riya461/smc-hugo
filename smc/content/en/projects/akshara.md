+++
title = 'Akshara Mazha'
draft = false
val = 'Mobile'
img = '/images/unnamed.png'
source = 'https://gitlab.com/jishnu7/Akshara-Mazha'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='https://play.google.com/store/apps/details?id=in.androidtweak.rain'
doc='none'
details = 'none'
+++
Open source live wallpaper with Malayalam letters streaming down the screen like you have seen in the popular movie Matrix. 