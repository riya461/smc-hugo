+++
title = 'Machine translation'
draft = false
val = 'Machine translation & Spelling checker'
img = 'none'
details = 'none'
source = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
use = 'https://translate.smc.org.in/'
+++
Machine translation between English and Malayalam. This machine translation system uses huggingface transformers with OpusMT language models for translation. 