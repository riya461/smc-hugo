+++
title = 'Indic-En'
img = '/images/gayathri-card.png'
draft = false
val = 'Web Extensions'
use = 'none'
tools = 'none'
firefox = 'https://addons.mozilla.org/en-US/firefox/addon/indicen/'
chrome = 'https://chrome.google.com/webstore/detail/indic-en/fbbhbgpgjfjdncnealckbfdmieafpgon'
details = 'https://subinsb.com/indicen/'
source = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
+++
This addon will convert Malayalam, Hindi & Kannada webpages to Manglish, Hinglish & Kanglish respectively. For people who understands the language, but can't read the script. 