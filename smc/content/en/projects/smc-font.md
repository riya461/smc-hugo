+++
title = 'SMC Fonts'
draft = false
val = 'Fonts & Script'
img = 'none'
details = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
use = 'https://smc.org.in/fonts/'
source = 'https://gitlab.com/smc/fonts'
+++
SMC released and maintains some of the popular fonts of Malayalam language, Including Manjari, Gayathri, Chilanka, Rachana, AnjaliOldLipi, Keraleeyam, Uroob, Suruma, Meera and so on. 