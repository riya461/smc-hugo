+++
title = 'Text similarity'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/text-similarity'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'
+++
This module will compare two texts for their similarity. Based on the similarity it will give a number between 0 and 1. 1 means both text are similary. 0 means texts are completely different. A value in between 0 and 1 indicates how much they are similar. The algorithm uses an n-grams model and cosine similarity. 