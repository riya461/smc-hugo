+++
title = 'Dhvani'
draft = false
val = 'Accessibility'
img = 'none'
firefox = 'none'
chrome = 'none'
source = 'none'
use = 'none'
tools = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'http://dhvani.sourceforge.net/'
+++

Dhvani is a text to speech system designed for Indian Languages. It is helpful for the visually challenged users as a screenreader in their mother tongue. Currently dhvani is capable of generating intelligible speech for Bengali, Gujarati, Hindi, Kannada, Malayalam, Marathi, Oriya, Panjabi, Tamil, Telugu, Pashto(experimental)
