+++
title = 'Magisk Malayalam Fonts'
img = '/images/unnamed.png'
draft = false
val = 'Mobile'
firefox='none'
chrome='none'
tools = 'none'
use = 'none'
details='none'
howto='https://blog.smc.org.in/installing-smc-fonts-on-android-devices/'
download = 'https://gitlab.com/smc/magisk-malayalam-fonts/-/releases'
playstore='none'
fdroid='none'

doc='none'
source = 'https://gitlab.com/smc/magisk-malayalam-fonts'
+++
 Set the default Malayalam font for rooted Android devices. 