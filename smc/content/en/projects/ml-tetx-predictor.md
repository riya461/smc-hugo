
+++
title = 'Malayalam text prediction system'
val = 'Malayalam Linguistics'
draft = false
img = 'none'
details = 'https://predict.smc.org.in/'
source = 'none'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
+++
Markov chain based Malayalam text prediction system.
