+++
title = 'indicstemmer'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/indicstemmer'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'

summary="experimental malayalam stemmer"
+++


LibIndic's stemmer module may be used to extract stems of the words in a sentence. It is implemented in a rule-based model and follows iterative suffix stripping to handle multiple levels of inflection. Right now, it supports Malayalam language only. 