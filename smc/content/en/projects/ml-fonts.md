+++
title = 'Malayalam fonts'
val = 'Web Extensions'
draft = false
img = 'none'
details = 'none'
source = 'none'
use = 'none'
tools = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
firefox = 'https://addons.mozilla.org/en-US/firefox/addon/malayalam-fonts/'
chrome = 'https://chrome.google.com/webstore/detail/malayalam-font/jgdfkpocgoliiikomkacimfdkedjdgfo'
+++
This addon will help you to choose Unicode Malayalam fonts for the websites you visit, without installing anything on your phone or computer. 