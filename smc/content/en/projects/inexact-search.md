+++
title = 'Inexact Search'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/inexactsearch'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'

summary = "Approximate String Search"
+++
By mixing both written like(edit distance) and sounds like(soundex), we achieve an efficient aproximate string searching. This application is capable of cross language string search too. That means, you can search Hindi words in Malayalam text. If there is any Malayalam word, which is approximate transliteration of hindi word, or sounds alike the hindi words, it will be returned as an approximate match. The "written like" algorithm used here is a bigram average algorithm. 