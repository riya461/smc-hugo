+++
title = 'ml2en'
draft = false
val = 'Web'
tools = 'https://nadh.in/code/ml2en/'
source = 'https://github.com/knadh/ml2en/'
img = 'none'
details = 'none'
use = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
+++
A simple online tool to convert Malayalam text to Manglish. For people who understands Malayalam, but can't read the script. 