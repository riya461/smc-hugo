+++
title = 'ibus-sharada-braille'
draft = false
val = 'Accessibility'
img = 'none'
source = 'none'
firefox = 'none'
chrome = 'none'
use = 'none'
tools = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://github.com/Nalin-x-Linux/ibus-sharada-braille'
+++

ibus-sharada-braille(ISB) is an ibus input engine based on six key approach of braille. It currently supports seven languages English, Malayalam, Hindi, Kannada, Tamil, French, and Spanish. 