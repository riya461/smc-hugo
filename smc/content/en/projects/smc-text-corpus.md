+++
title = 'SMC Text corpus'
draft = false
val = 'Corpus and data collections'
img = 'none'
details = 'none'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
source = 'http://gitlab.com/smc/corpus'
+++
A collection of Malayalam text content for various research purpose, curated and released in Creative commons license. 