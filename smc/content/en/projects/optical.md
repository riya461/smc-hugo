+++
title = 'Optical Character recognition'
draft = false
val = 'Fonts & Script'
img = 'none'
firefox = 'none'
chrome = 'none'
details = 'none'
tools = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
use = 'https://ocr.smc.org.in/'
source = 'https://gitlab.com/smc/tesseract-ocr-web'
+++
 Scan and OCR documents in Malayalam. OCR recognizes the text in the document and gives plain text version. This is a tesseract-ocr based tool 