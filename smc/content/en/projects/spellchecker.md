+++
title = 'Spellchecker'
draft = false
val = 'Machine translation & Spelling checker'
img = 'none'
details = 'none'
source = 'https://github.com/libindic/spellchecker'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
tools = 'none'
firefox = 'none'
chrome = 'none'
use = 'https://morph.smc.org.in/spellcheck.html'
+++
 LibIndic's spellchecker module may be used to detect spelling mistakes in a word. If a spelling mistake is found, it generates valid root words as suggestions that have a higher probability being the word user actually intended to us