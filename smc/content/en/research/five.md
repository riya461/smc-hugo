+++
title = 'A quantitative analysis of the morphological complexity of Malayalam'
draft = false
val = 'Published Papers'
mlphon = 'none'

read='none'
analyse = 'none'
blog = 'https://kavyamanohar.com/post/malayalam-morphological-complexity/'
author = 'Kavya Manohar'
link = 'https://rdcu.be/b6RVa'
code = 'https://gitlab.com/kavyamanohar/malayalam-ttr'
+++

By measuring the morphological complexity and comparing it against the same measurements for Dravidian languages and European languages, this paper show that Malayalam is morphologically more complex than them. Published at the 23rd International Conference on Text, Speech and Dialogu on September 8-10, 2020, Brno, Czech Republic.
