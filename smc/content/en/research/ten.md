+++
title = 'A finite state Transducer based approach for Malayalam phonetics analysis '
draft = false
val = 'Malayalam Computational Linguistics'
mlphon = 'none'
code = 'none'
blog = 'none'
analyse = 'https://phon.smc.org.in/'
author = ' Kavya Manohar'
read= 'https://kavyamanohar.com/post/malayalam-phonetic-analyser/'
link = 'none'
+++

A system to analyse the phonology of Malayalam. Published on October 2, 2018   
