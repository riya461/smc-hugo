+++
title = 'Finite State Transducer based Morphology analysis for Malayalam Language'
draft = false
val = 'Published Papers'
author = 'Santhosh Thottingal'
mlphon = 'none'
code = 'none'

read='none'
analyse = 'none'
blog = 'none'
link = 'https://www.aclweb.org/anthology/W19-6801/'
+++

Proceedings of the 2nd Workshop on Technologies for MT of Low Resource Languages, European Association for Machine Translation, August 20, Dublin, Ireland. 