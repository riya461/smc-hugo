+++
title = 'Phonetic description of Malayalam consonants '
draft = false
val = 'Malayalam Computational Linguistics'
mlphon = 'none'
code = 'none'
blog = 'none'
analyse = 'none'
author = ' Kavya Manohar'
read= 'https://kavyamanohar.com/post/malayalam-ipa-consonants/'
link = 'none'
+++

A study of phonetic description of Malayalam consonants based on existing methods and IPA. Published on January 18, 2020 