+++
title = 'Spiral splines in typeface design: A case study of Manjari Malayalam typeface  '
draft = false
code = 'none'
blog = 'none'

read='none'
analyse = 'none'
mlphon = 'none'
val = 'Published Papers'
author = 'Santhosh Thottingal, Kavya Manohar'
link = 'https://thottingal.in/documents/Spiral-Splines-Manjari.pdf'
+++


Proceedings of <a href = "http://www.typoday.in/">[Typoday 2018](http://www.typoday.in/)</a>, University of Mumbai. March 2018. 