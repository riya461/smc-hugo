const postcssPresetEnv = require('postcss-preset-env');

module.exports = {
    plugins: [
        postcssPresetEnv({
            /* pluginOptions */
            features: {
                "nesting-rules": true,
                "cascade-layers": true,
                "light-dark-function": true,
                "oklab-function": true,
                "color-function": true,
                "logical-viewport-units": true,
                "prefers-color-scheme-query": true,
                "font-variant-property": true,
            },
        })
    ]
}