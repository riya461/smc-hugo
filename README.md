# SMC new website

Currently available at [https://smc.org.in](https://smc.org.in).

## Development

Initial setup:

```bash
make dev
```

and go to [http://localhost:1313](http://localhost:1313)

## Deployment

```bash
make build
```
