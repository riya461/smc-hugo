.phony: build dev assets clean fonts
THEME_DIR = smc/themes/basic
FONT_NAMES=gayathri anjalioldlipi rachana meera manjari raghumalayalamsans dyuthi keraleeyam uroob chilanka karumbi suruma malini nupuram
all: build
FONTS=$(FONT_NAMES:%=smc/public/downloads/fonts/%)
FONTS_METADATA := $(CURDIR)/smc/assets/fonts.csv

init:
	npm install -D postcss postcss-cli autoprefixer postcss-preset-env

build: init fonts
	hugo --source smc

dev: init fonts
	hugo server --buildDrafts --source smc

fonts: $(FONTS)

smc/public/downloads/fonts/nupuram:
	$(eval FONT_NAME := nupuram)
	@mkdir -p $@
	@cd $@ && \
	version=$$(curl -sSL "https://gitlab.com/api/v4/projects/smc%2Ffonts%2F$(FONT_NAME)/repository/tags?order_by=updated" | jq '.[].name' | head -1 | tr -d '"') && \
	echo "$(FONT_NAME),$${version}" >> $(FONTS_METADATA) && \
	echo "Building $(FONT_NAME) version $${version}" && \
	url="https://gitlab.com/smc/fonts/Nupuram/-/jobs/artifacts/$${version}/download?job=build" && \
	wget -q $$url -O artifacts.zip && \
	unzip -q artifacts.zip && \
	mv fonts/* . && \
	rm -rf fonts artifacts.zip && \
	zip -d Nupuram.zip fonts/Nupuram/ufo/\*
	# Remove all folders named 'ufo' from subdirectories
	find $@ -type d -name ufo -exec rm -rf {} +

smc/public/downloads/fonts/malini:
	$(eval FONT_NAME := malini)
	@mkdir -p $@
	@cd $@ && \
	version=$$(curl -sSL "https://gitlab.com/api/v4/projects/smc%2Ffonts%2F$(FONT_NAME)/repository/tags?order_by=updated" | jq '.[].name' | head -1 | tr -d '"') && \
	echo "$(FONT_NAME),$${version}" >> $(FONTS_METADATA) && \
	echo "Building $(FONT_NAME) version $${version}" && \
	url="https://gitlab.com/smc/fonts/Malini/-/jobs/artifacts/$${version}/download?job=build" && \
	wget -q $$url -O artifacts.zip && \
	unzip -q artifacts.zip && \
	mv fonts/Malini-$${version}.zip Malini.zip && \
	mv fonts/Malini/* . && \
	rm -rf fonts Malini artifacts.zip ufo

smc/public/downloads/fonts/%:
	$(eval FONT_NAME := $(@F))
	@mkdir -p $@
	@cd $@ && \
	version=$$(curl -sSL "https://gitlab.com/api/v4/projects/smc%2Ffonts%2F$(FONT_NAME)/repository/tags?order_by=updated" | jq '.[].name' | head -1 | tr -d '"') && \
	echo "$(FONT_NAME),$${version}" >> $(FONTS_METADATA) && \
	echo "Building $(FONT_NAME) version $${version}" && \
	url="https://gitlab.com/smc/fonts/$(FONT_NAME)/-/jobs/artifacts/$${version}/download?job=build-tag" && \
	wget -q $$url -O artifacts.zip && \
	unzip -q artifacts.zip && \
	mv build/* . && \
	mv $(FONT_NAME)-$${version}.zip $(FONT_NAME).zip && \
	rm -rf build artifacts.zip


clean:
	@rm -rf smc/public
	@rm -f $(FONTS_METADATA)